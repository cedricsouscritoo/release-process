# **Release Process**

## Script steps : 

1. Create the APP CORE + OPS (AOX) release note

2. Close the current release on theses two projects

3. Create the APP PRO release note

4. Close the current release on this project

## Execute the script :

```sh
$ python release-process.py
```

## Constants :
```
# Auth User
user = "cedric.molla@papernest.com"
# Auth Token
token = "e2h4WsBMQzvKgCt61mgEE205"

# Atlassian server
server = "souscritoo.atlassian.net"

# Confluence space key for REP release notes
space_rep = "RNR"
# Confluence space key for APP release notes
space_app = "RNA"

# Complete Jira API URL
jira_url = "https://" + server + "/rest/api/3/"
# Complete Confluence API URL
wiki_url = "https://" + server + "/wiki/rest/api/"
# Complete Jirz API URL for issues
issue_url = "https://" + server + "/browse/"
```

## Tasks types :
```
# Types we want to keep in our releases notes
types = ["Bug", "User story", "Technical story", "Technical debt"]
# Treatment to group some specific types into the types we want
# Learn this like : "old type": "new type"
tr = {
    "Bug": "Bug",
    "Story": "User story",
    "User story": "User story",
    "Epic": "User story",
    "Task": "User story",
    "Sub-task": "User story",
    "Technical story": "Technical story",
    "Technical debt": "Technical debt"
}
```
