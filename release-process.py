from requests import request
from datetime import date

# Auth User
user = "cedric.molla@papernest.com"
# Auth Token
token = "e2h4WsBMQzvKgCt61mgEE205"

# Atlassian server
server = "souscritoo.atlassian.net"

# Confluence space key for REP release notes
space_rep = "RNR"
# Confluence space key for APP release notes
space_app = "RNA"

# Complete Jira API URL
jira_url = "https://" + server + "/rest/api/3/"
# Complete Confluence API URL
wiki_url = "https://" + server + "/wiki/rest/api/"
# Complete Jirz API URL for issues
issue_url = "https://" + server + "/browse/"

today = date.today()

# Types we want to keep in our releases notes
types = ["Bug", "User story", "Technical story", "Technical debt"]
# Treatment to group some specific types into the types we want
# Learn this like : "old type": "new type"
tr = {
    "Bug": "Bug",
    "Story": "User story",
    "User story": "User story",
    "Epic": "User story",
    "Task": "User story",
    "Sub-task": "User story",
    "Technical story": "Technical story",
    "Technical debt": "Technical debt"
}

# Treatment for APPCO & APPOPS (AOX)
issues = []
projects = ["APPCO", "AOX"]
for project in projects:
    url = jira_url + "project/" + project + "/version"
    params = {"status": "unreleased"}
    version = (request("GET", url, params=params, auth=(user, token))).json()

    if version["total"] == 1:
        print(project + " current unreleased version: " + version["values"][0]["name"])
        url = jira_url + "version/" + version["values"][0]["id"] + "/unresolvedIssueCount"
        versionIssues = (request("GET", url, auth=(user, token))).json()

        print("Issues unresolved: " + str(versionIssues["issuesUnresolvedCount"]) + "/" +
              str(versionIssues["issuesCount"]))
        if versionIssues["issuesCount"] > 0 and versionIssues["issuesUnresolvedCount"] == 0:

            print("Ok I can released !")
            url = jira_url + "version/" + version["values"][0]["id"]
            params = {"released": True, "releaseDate": str(today)}
            request("PUT", url, json=params, auth=(user, token)).json()

            url = (jira_url + "search?jql=project=" + project + " AND fixVersion=" +
                   version["values"][0]["name"])
            versionIssues = (request("GET", url, auth=(user, token))).json()

            for issue in versionIssues["issues"]:
                issues.append({
                    "key": issue["key"],
                    "link": issue_url + issue["key"],
                    "title": issue["fields"]["summary"],
                    "type": issue["fields"]["issuetype"]["name"]
                })

if len(issues) > 0:
    content = ("<p>Date : <time datetime=\"" + str(today) +
               "\" class=\"date-past\">" + str(today) + "</time></p>")
    content += "<p>Issues : " + str(len(issues)) + "</p>"
    current_type = None
    for issue in sorted(issues, key=lambda i: types.index(tr[i["type"]])):
        if current_type != tr[issue["type"]]:
            current_type = tr[issue["type"]]
            content += "<br /><p><strong>" + current_type + "</strong></p>"
        content += ("<p>* <a href=\"" + issue["link"] + "\">[" +
                    issue["key"] + "]</a> - " + issue["title"] + "</p>")

    print("Creation of the Release Note")
    release_note = {
        "type": "page",
        "space": {
            "key": space_app
        },
        "title": "Release notes App - " +
                 version["values"][0]["name"] + " - " + str(today),
        "body": {
            "storage": {
                "value": content,
                "representation": "storage"
            }
        }
    }
    print(content)
    request("POST", wiki_url + "content", json=release_note, auth=(user, token))
    print("Release Note create with success !")

# Treatment for PART
project = "PART"
url = jira_url + "project/" + project + "/version"
params = {"status": "unreleased"}
version = (request("GET", url, params=params, auth=(user, token))).json()

if version["total"] == 1:
    print(project + " current unreleased version: " + version["values"][0]["name"])
    url = jira_url + "version/" + version["values"][0]["id"] + "/unresolvedIssueCount"
    versionIssues = (request("GET", url, auth=(user, token))).json()

    print("Issues unresolved: " + str(versionIssues["issuesUnresolvedCount"]) + "/" +
          str(versionIssues["issuesCount"]))
    if versionIssues["issuesCount"] > 0 and versionIssues["issuesUnresolvedCount"] == 0:

        print("Ok I can released !")
        url = jira_url + "version/" + version["values"][0]["id"]
        params = {"released": True, "releaseDate": str(today)}
        request("PUT", url, json=params, auth=(user, token)).json()

        url = (jira_url + "search?jql=project=" + project + " AND fixVersion=" +
               version["values"][0]["name"])
        versionIssues = (request("GET", url, auth=(user, token))).json()

        issues = []
        for issue in versionIssues["issues"]:
            issues.append({
                "key": issue["key"],
                "link": issue_url + issue["key"],
                "title": issue["fields"]["summary"],
                "type": issue["fields"]["issuetype"]["name"]
            })

        if len(issues) > 0:
            content = ("<p>Date : <time datetime=\"" + str(today) +
                       "\" class=\"date-past\">" + str(today) + "</time></p>")
            content += "<p>Issues : " + str(len(issues)) + "</p>"
            current_type = None
            for issue in sorted(issues, key=lambda i: types.index(tr[i["type"]])):
                if current_type != tr[issue["type"]]:
                    current_type = tr[issue["type"]]
                    content += "<br /><p><strong>" + current_type + "</strong></p>"
                content += ("<p>* <a href=\"" + issue["link"] + "\">[" +
                            issue["key"] + "]</a> - " + issue["title"] + "</p>")

            print("Creation of the Release Note")
            release_note = {
                "type": "page",
                "space": {
                    "key": space_rep
                },
                "title": "Release notes REP - " +
                         version["values"][0]["name"] + " - " + str(today),
                "body": {
                    "storage": {
                        "value": content,
                        "representation": "storage"
                    }
                }
            }
            print(content)
            request("POST", wiki_url + "content", json=release_note, auth=(user, token))
            print("Release Note create with success !")
